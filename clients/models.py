from django.db import models

from congratulations.helper import GENDER


class Client(models.Model):
    fio = models.CharField(max_length=50)
    email = models.EmailField()
    address = models.TextField(max_length=100)
    gender = models.CharField(choices=GENDER, max_length=20)
    enrollment_date = models.DateField()

    def __str__(self):
        return self.fio

