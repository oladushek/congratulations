from django.contrib import admin
from clients.models import Client
from holidays.models import CongratulatedCustomers


class CongratulatedCustomersInline(admin.TabularInline):
    model = CongratulatedCustomers


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('fio', 'email', 'gender',)
    inlines = [CongratulatedCustomersInline]
