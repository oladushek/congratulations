from django import forms

from congratulations.helper import GENDER


class GetClientsForm(forms.Form):
    from_date = forms.DateField()
    until_date = forms.DateField()
    gender = forms.ChoiceField(choices=GENDER)
