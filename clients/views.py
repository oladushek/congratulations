from django.core import serializers
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from clients.forms import GetClientsForm
from clients.models import Client


def index(request):

    if request.method == 'POST':
        form = GetClientsForm(request.POST)
        if form.is_valid():
            clients = Client.objects.filter(
                enrollment_date__gte=form.cleaned_data['from_date'],
                enrollment_date__lte=form.cleaned_data['until_date'],
                gender=form.cleaned_data['gender']
            )
            data = serializers.serialize('xml', clients)
            return HttpResponse(data, content_type='application/xml')
        else:
            return HttpResponseRedirect('/')

    else:
        form = GetClientsForm()
        return render(
            request,
            'index.html',
            context={'form': form},
        )
