from django.db import models

from clients.models import Client
from congratulations.helper import GENDER


class Holiday(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    day = models.DateField()
    gender = models.CharField(choices=GENDER, max_length=20, null=True, blank=True)

    def __str__(self):
        return self.name


class CongratulatedCustomers(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    holiday = models.ForeignKey(Holiday, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
