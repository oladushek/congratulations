from django.contrib import admin
from holidays.models import Holiday, CongratulatedCustomers


class CongratulatedCustomersInline(admin.TabularInline):
    model = CongratulatedCustomers


@admin.register(Holiday)
class HolidayAdmin(admin.ModelAdmin):
    list_display = ('name', 'day', 'gender',)
    inlines = [CongratulatedCustomersInline]


@admin.register(CongratulatedCustomers)
class CongratulatedCustomersAdmin(admin.ModelAdmin):
    list_display = ('client', 'holiday', 'created_at',)
