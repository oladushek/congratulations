## Launch of the project  
Run the following commands to run the project.  
1. pip install -r requirements.txt  
2. in the file congratulations/settings.py in the DATABASES parameters configure the data to connect to PostgreSQL
3. python manage.py makemigrations  
4. python manage.py migrate  
5. python manage.py loaddata holiday
6. python manage.py createsuperuser  

## Get client list in XML  
1. Open page http://127.0.0.1:8000/  
2. Enter data  
3. Press OK  
4. XML will load on the main page  

## An example of an SQL query that selects girls who are not currently congratulated on March 8, but congratulated on the new year.

SELECT  
    c.fio, c.email  
FROM  
    clients_client c  
INNER JOIN  
    holidays_congratulatedcustomers h  
ON  
    c.id=h.client_id  
WHERE  
    h.holiday_id = 'New Year''s Day' AND h.client_id NOT IN (  
    SELECT  
        h.client_id  
    FROM  
        holidays_congratulatedcustomers h  
    WHERE  
        h.holiday_id = 'Women''s Day'  
    GROUP BY  
        h.client_id)  
GROUP BY c.fio, c.email;  
